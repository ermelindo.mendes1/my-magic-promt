age() {

  read -p "Entrez votre âge : " age

  if [ "$age" -lt 0 ]; then
    echo "Votre âge ne peut pas être négatif."
    return
  fi

  if [ "$age" -ge 18 ]; then
    echo "Vous êtes majeur."
  else
    echo "Vous êtes mineur."
  fi
}
