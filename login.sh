source profil.sh

login() {
    read -p "Entrez votre adresse e-mail : " email
    read -s -p "Entrez votre mot de passe : " password

    if [[ -n ${profil["$email"]} && ${profil["$email"]} == *"$password" ]]; then
        echo "Connexion réussie."
        currentUser="$email" 
    else
        echo "Connexion échouée. Adresse e-mail ou mot de passe incorrect."
fi
}
