The "My Magic Prompt" program is an interactive command-line application that allows you to perform various tasks and common operations in a terminal environment. To access the prompt, you will need to log in with specific username and password credentials. The program is entirely contained within the ~/my-magic-prompt/main.sh directory.

The main features of the prompt include:

help: Displays a list of available commands that you can use.

ls: Lists visible and hidden files and folders in the current directory.

rm: Allows you to delete a file.

rmd or rmdir: Allows you to delete a directory.

about: Displays a description of the program.

version, --v, or vers: Displays the version of the prompt.

age: Prompts you for your age and informs you whether you are of legal age.

quit: Allows you to exit the prompt.

profil: Displays all information about yourself, including your first name, last name, age, and email.

passw: Allows you to change the password with a confirmation request.

cd: Allows you to navigate to a directory you have just created or return to a previous directory.

pwd: Indicates the current working directory.

hour: Provides the current time.

*: Indicates an unknown command.

httpget: Allows you to download the HTML source code of a web page and save it to a specific file. The prompt asks for the filename.

smtp: Enables you to send an email with an address, subject, and message body.

open: Opens a file directly in the VIM editor, even if the file does not exist.

This program offers a variety of useful features for common file management tasks, directory navigation, password management, email sending, and other common operations in a terminal environment.
