httpget() {
  
  read -p "Entrez l'URL de la page web : " url

  read -p "Entrez le nom du fichier de sortie : " output_file

  curl -o "$output_file" "$url"

  if [ $? -eq 0 ]; then
    echo "Téléchargement terminé avec succès. Le code source HTML a été enregistré dans '$output_file'."
  else
    echo "Une erreur s'est produite lors du téléchargement de la page web."
  fi
}