source profil.sh
source login.sh

changePassword() {
    local newPassword="$2"

    if [[ -n $currentUser && -n ${profil["$currentUser"]} ]]; then
        read -s -p "Entrez le nouveau mot de passe : " newPassword
        echo
        read -s -p "Confirmez le nouveau mot de passe : " confirmPassword
        echo

        if [[ "$newPassword" == "$confirmPassword" ]]; then
            IFS=':' read -ra userData <<< "${profil["$currentUser"]}"
            email="${userData[3]}"
            echo "Contenu de profil[\$currentUser] : ${profil["$currentUser"]}"
            userData[4]="$newPassword"
            userInfo="${profil["$email"]}"
            firstName="${userData[0]}"
            lastName="${userData[1]}"
            age="${userData[2]}"
            newLine="$firstName:$lastName:$age:$email:$newPassword"


        sed -i "s/$userInfo/$newLine/" profil.sh

            echo "Mot de passe modifié avec succès pour l'adresse e-mail : $email."
        else
            echo "Les mots de passe ne correspondent pas. Mot de passe inchangé."
        fi
    else
        echo "Utilisateur introuvable pour l'adresse e-mail : $email."
    fi
}
