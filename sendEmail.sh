sendEmail() {

read -p "Entrez l'adresse e-mail du destinataire : " email
read -p "Entrez le sujet de l'e-mail : " subject
read -p "Entrez le corps du message : " body

echo -e "Subject: $subject\n$body" | msmtp "$email"

if [ $? -eq 0 ]; then
    echo "Courriel envoyé avec succès à $email."
else
    echo "Une erreur s'est produite lors de l'envoi de ce courriel."
fi

}
