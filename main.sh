#!/usr/bin/bash
source quit.sh
source list.sh
source changeDirectory.sh
source removeFile.sh
source removeDirectory.sh
source printwd.sh
source aboutCommand.sh
source hour.sh
source age.sh
source version.sh
source unknownCommand.sh
source help.sh
source httpget.sh
source openFile.sh
source sendEmail.sh
source login.sh
source profil.sh
source changePassword.sh

cmd() {
  cmd=$1
  argv=$*

  case "${cmd}" in
    quit | exit ) quit;;
    ls ) shift; list $*;;
    cd ) shift; changeDirectory $*;;
    rm ) shift; removeFile $*;;
    rmdir ) shift; removeDirectory $*;;
    pwd ) printwd;;
    about ) aboutCommand;;
    hour ) hour;;
    age ) age;;
    version | --v | vers ) version;;
    help ) help;;
    httpget ) shift; httpget $*;;
    open ) shift; openFile $*;;
    smtp ) shift; sendEmail $*;;
    passw ) changePassword;;
    * ) unknownCommand "${cmd}";;
  esac
}

main() {

  success=false  # Variable pour suivre l'état de la connexion

  while [ "$success" = false ]; do
    login  # Appelez la fonction de connexion
    success=true

    if [ "$success" = true ]; then
      break  # Sortez de la boucle si la connexion réussie
    fi
  done

  lineCount=1

  while [ 1 ]; do
    date=$(date +%H:%M)
    echo -ne "${date} - [\033[31m${lineCount}\033[m] - \033[33mbrunel\033[m ~ ☠️  ~ "
    read string

    cmd $string
    lineCount=$(($lineCount+1))
  done
}

main
