declare -A profil

profil=(
    ['brunel@test.com']='Brunel:lopes:30:brunel@test.com:teste1'
    ['test@test.com']='teste:teste:25:test@test.com:teste'
)

myProfil() {
  if [[ -n $currentUser && -n ${profil["$currentUser"]} ]]; then
    userInfo="${profil["$currentUser"]}"
    IFS=':' read -ra userData <<< "$userInfo"

    firstName="${userData[0]}"
    lastName="${userData[1]}"
    age="${userData[2]}"
    email="${userData[3]}"

    echo "Nom : $firstName"
    echo "Prénom : $lastName"
    echo "Âge : $age"
    echo "Adresse e-mail : $email"
  else
    echo "Vous n'êtes pas connecté ou votre profil est introuvable."
  fi
}
