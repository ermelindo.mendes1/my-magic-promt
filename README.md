# My Magic Prompt

## My Magic Prompt is a Bash-based command prompt that allows you to perform various actions. 

Before getting started, you need to configure the "profile" file with your email, first name, last name, age, email again, and password, within the "profil.sh" file's array, as shown in the example below:


['brunel@test.com']='Brunel:lopes:30:brunel@test.com:teste1'

To launch the prompt, you should execute the command ./main.sh. The prompt will prompt you for your email and password, after which you will be logged in and able to perform various actions:

The "help" command: Displays the list of available commands.

"ls": Lists both visible and hidden files and directories.

"rm": Deletes a file.

"rmdir": Deletes a directory.

"about": Provides a description of the program.

"version" or "--v" or "vers": Displays the prompt's version.

"age": Prompts for your age and tells you whether you are a minor or an adult.

"quit": Exits the prompt.

"profil": Displays all your information, including first name, last name, age, and email.

"passw": Allows you to change your password with confirmation.

"cd": Navigates to a newly created folder or returns to a previous one.

"pwd": Indicates the current working directory.

"hour": Provides the current time.

"httpget": Downloads the HTML source code of a web page and saves it in a specific file. The prompt will ask for the filename.

"smtp": Enables you to send an email with an address, subject, and message body. To use email functionality, you need to install "msmtp" and configure it with your Gmail account. Here's how to do it:

Execute the following command: sudo apt-get install ssmtp.
Configure the file ~/.msmtprc with your Gmail account information, as shown in the example configuration below:
bash
Copy code
account default
host smtp.gmail.com
port 465
protocol smtp
auth on
from your_email@gmail.com
user your_email@gmail.com
password your_password
Set this account as the default account.
"open": Opens a file directly in the VIM editor, even if the file doesn't exist.

For more information on configuring "msmtp," you can refer to this link: Configure MSMTP.
**https://wiki.archlinux.org/title/Msmtp**